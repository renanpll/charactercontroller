﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public Animator anim;

    bool isWalking = true;
    bool isCrouch = false;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        Move();
    }

    void Move()
    {

        //Change between Walk and Run
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isWalking = !isWalking;
        }

        //Crouch
        if (Input.GetKeyDown(KeyCode.C))
        {
            isCrouch = !isCrouch;
            anim.SetBool("Crouch", isCrouch);
        }

        if (isWalking && !isCrouch)
        {
            //Walk
            anim.SetFloat("Foward", Mathf.Clamp(Input.GetAxis("Vertical"), -0.5f, 0.5f));
            anim.SetFloat("Turn", Mathf.Clamp(Input.GetAxis("Horizontal"), -.5f, 0.5f));
        }
        else
        {
            //Run
            anim.SetFloat("Foward", Input.GetAxis("Vertical"));
            anim.SetFloat("Turn", Input.GetAxis("Horizontal"));
        }
    }
}
